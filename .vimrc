"  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>	"
" Griffin's .vimrc 08/21/2013					"
"								"
" Designed to be close-to-stock and portable to quickly		"
" implement in new environments. This file as well as my Vim	"
" plugins can be found at:					"
"								"
" http://bitbucket.org/launchcraft/vim-config			"
"								"
" me@griffinmoe.com * http://griffinmoe.com/			"
"  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>  <>	"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Settings						"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Use Vim settings, rather than Vi settings
set nocompatible

" Set up Pathogen
runtime bundle/vim-pathogen/autoload/pathogen.vim
filetype off
call pathogen#infect()
call pathogen#helptags()

" Enable file type detection
filetype plugin indent on

" Rebind arrow keys for window movement
noremap <Up> <C-w>k
noremap <Down> <C-w>j
noremap <Left> <C-w>h
noremap <Right> <C-w>l

" Keep 50 lines of command line history
set history=50

" Adjust the tab size
set shiftwidth=4
set softtabstop=4

" Turn on auto-ident
set autoindent
set smartindent

" Do incremental searching
set incsearch

" Improve graphics response in some terminals
set ttyfast

" Auto-read file if it has been edited externally
set autoread

" :VimUpdate pulls new Git patches for our plugins
command VimUpdate execute "!~/.vim/update.sh"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Visual + Colors						"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable 256-color support, we also like dark backgrounds
set t_Co=256
set background=dark

" Molokai colorscheme
colorscheme molokai
let g:rehash256=1
let g:molokai_original=1

" Set the font to something pretty 
set guifont=Menlo:h11

" Configure the statusline
set laststatus=2
set statusline=%F%m\ %r%h%w\ \|\ \ [%{&ff}:%Y]\ %=[%04l,%04v]\ [%p%%]
au InsertEnter * hi StatusLine term=reverse ctermbg=112 gui=undercurl guisp=Green
au InsertLeave * hi StatusLine term=reverse ctermfg=238 ctermbg=253 gui=bold,reverse

" Offset the screen while scrolling to see 3 lines below the cursor
set scrolloff=3

" Turn on syntax-highlighting
syntax on
set hlsearch

" Turn on visual bells
set visualbell
set errorbells

" Show the cursor position all the time
set ruler

" Display incomplete commands
set showcmd

" Display line numbers by default
set number

" Turn off word wrap
set nowrap

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin Settings + Configuration				"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Vim-LaTeX
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
let g:Tex_CompileRule_pdf='xelatex -interaction=nonstopmode $*'
