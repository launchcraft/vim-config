#This script goes through all of the git repositories within
#the Vim bundle and attempts to pull the latest commits
#
#Written by Griffin Moe 1/21/13

echo "Updating Vim packages:\n"

cd ~/.vim/bundle
for item in *
do
   cd $item
   printf "Checking %s...\n" $item
   git pull
   cd ..
done
